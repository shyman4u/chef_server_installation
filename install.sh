#!/bin/bash

# Enable strict mode
set -euo pipefail
# Reset locale settings
export LC_ALL=en_US.UTF-8

# validate we have variables
if [ "$#" -ne 1 ]; then
    IP=$(ip -o ro get 8.8.8.8 | awk '{print $7}')
else
    IP=$1;
fi

# Set proxy variables
export HTTPS_PROXY="http://one.proxy.att.com:8080";
export HTTP_PROXY="http://one.proxy.att.com:8080";
export NO_PROXY=127.0.0.1,$IP;
export https_proxy="http://one.proxy.att.com:8080";
export http_proxy="http://one.proxy.att.com:8080";
export no_proxy=127.0.0.1,$IP;

CHEF_ENV=production;
CHEF_SERVER_VERSION=11.1.6
CHEF_SERVER_PKG_URL="https://www.chef.io/chef/download-server?p=ubuntu&pv=12.04&m=x86_64&v=${CHEF_SERVER_VERSION}";
CHEF_SERVER_DEB_FILE=chef-server.deb
CHEF_CLIENT_VERSION=11.18.6
CHEF_CLIENT_PKG_URL="https://www.chef.io/chef/download?p=ubuntu&pv=12.04&m=x86_64&v=${CHEF_CLIENT_VERSION}";
CHEF_CLIENT_DEB_FILE=chef-client.deb
TEMP_DIR=/tmp;
#CHEF_DEB_FILE=$TEMP_DIR/${CHEF_PKG_URL##*/};
ADMIN_CHEF_HOME=/root/.chef/;
CHEF_ADMIN_USER=admin;
UNIX_ADMIN_USER=root;
URL="https://${IP}/";

# currently this function is not neccessary
function wait_dpkg_lock() {
    for ((x = 0 ; x <= 100 ; x++)); do
        fuser /var/lib/dpkg/lock &>/dev/null || return 0
        sleep 1
    done
    return 1 
}

#############################################################################
	   	# Install chef server #
#############################################################################

function install_chef_server() {
    if [ ! -f $CHEF_SERVER_DEB_FILE ]; then
        wget -nv --directory-prefix=$TEMP_DIR $CHEF_SERVER_PKG_URL -O $CHEF_SERVER_DEB_FILE;
    fi;
    dpkg -i $CHEF_SERVER_DEB_FILE;
    
    mkdir -p /etc/chef-server/
    cat >/etc/chef-server/chef-server.rb<<EOF
server_name = "$IP"
api_fqdn server_name
nginx['url'] = "https://#{server_name}"
nginx['server_name'] = server_name
nginx['enable_non_ssl'] = true
nginx['non_ssl_port'] = 4000
lb['fqdn'] = server_name
bookshelf['vip'] = server_name
EOF
    chef-server-ctl reconfigure;
    
    install -d ${ADMIN_CHEF_HOME}
    install -m 600 /etc/chef-server/chef-validator.pem ${ADMIN_CHEF_HOME}
    install -m 600 /etc/chef-server/admin.pem ${ADMIN_CHEF_HOME}
    
    # Configure knife.
    cat >${ADMIN_CHEF_HOME}/knife.rb<<EOF
log_level                :info
log_location             STDOUT
node_name                '${CHEF_ADMIN_USER}'
client_key               '${ADMIN_CHEF_HOME}/admin.pem'
validation_client_name   'chef-validator'
validation_key           '${ADMIN_CHEF_HOME}/chef-validator.pem'
chef_server_url          '${URL}'
cache_type               'BasicFile'
syntax_check_cache_path  '${ADMIN_CHEF_HOME}/syntax_check_cache'
cache_options( :path =>  '${ADMIN_CHEF_HOME}/checksums' )
cookbook_path            ["./cookbooks"]
veryfy_api_cert          false
EOF
}

function install_chef_client() {
    if [ ! -f $CHEF_CLIENT_DEB_FILE ]; then
        wget -nv --directory-prefix=$TEMP_DIR $CHEF_CLIENT_PKG_URL -O $CHEF_CLIENT_DEB_FILE;
    fi;
    dpkg -i $CHEF_CLIENT_DEB_FILE;

    cat > /etc/chef/client.rb<<EOF
log_level              :info
log_location           STDOUT
chef_server_url        "https://${IP}"
Ohai::Config[:disabled_plugins] = ["rackspace"]
validation_client_name "chef-validator"

http_proxy "http://pxyapp.proxy.att.com:8080"
https_proxy "http://pxyapp.proxy.att.com:8080"
ENV['http_proxy'] = "http://pxyapp.proxy.att.com:8080"
ENV['https_proxy'] =  "http://pxyapp.proxy.att.com:8080"
ENV['HTTP_PROXY'] = "http://pxyapp.proxy.att.com:8080"
ENV['HTTPS_PROXY'] =  "http://pxyapp.proxy.att.com:8080"

no_proxy "localhost,*.attcompute.com,${IP%.*}.*"
ENV['no_proxy'] = "localhost,*.attcompute.com,${IP%.*}.*"
environment ${CHEF_ENV}
EOF

cp /etc/chef-server/chef-validator.pem /etc/chef/validation.pem

}


#############################################################################
				# clone chef-repo #
#############################################################################

function clone_chef_repo() {

    local repos=(
        'chef-repo:dev/juno'
        'deployment-data:ccpdev'
        'substructure:v2'
    )

    install -m700 -d ~/.ssh
    install -m600 key ~/.ssh/id_rsa 
    printf "Host *\n StrictHostKeyChecking no" > ~/.ssh/config
    
    #ssh-agent bash -c ' ssh-add key
    for remote in ${repos[@]}; do
        repo="${remote%%:*}"
        branch="${remote##*:}"
        git clone -b $branch ssh://git@codecloud.web.att.com:7999/st_ccp/${repo}.git /opt/${repo}
    done
}

#############################################################################
	   	#install berkshelf#
#############################################################################

function install_berkshelf() {
    # Configure for berks upload.
    mkdir -p /root/.berkshelf
    cat > /root/.berkshelf/config.json<<EOF
{
"ssl": {"verify": false}
}
EOF

    # ruby1.9.1 ruby1.9.1-dev should be installed
    gem install --no-ri --no-rdoc berkshelf
}

#############################################################################
	   	# run tasks specific to berks #
#############################################################################

function berkshelf_run() {
    :> /opt/chef-repo/Berksfile.lock
    berks install -b /opt/chef-repo/Berksfile
    # http_proxy= is workaround du to  berkshelf does not support NO_PROXY=
    http_proxy= berks upload -b /opt/chef-repo/Berksfile
}

#############################################################################
	   	# upload deployment-data to chef #
#############################################################################

function knife_run() {
    pushd /opt/chef-repo
    knife upload roles
    knife upload environments
    popd
}

#############################################################################
	        # run chef-client #
#############################################################################

function chef_client_run() {
    chef-client
}

#############################################################################
	   	#install substructure dependencies#
#############################################################################

function install_substructure_dependencies() {
    apt-get install -y python-prettytable
    apt-get install -y python-prettytable
    apt-get install -y python-yaml
}


#############################################################################
	   	# run substructure commands #
#############################################################################

#############################################################################
	   	# main #
      # organize function to blocks to avoid conflicts #
#############################################################################

{
    apt-get update
    apt-get install -y wget ruby1.9.1 ruby1.9.1-dev make g++
    if update-alternatives --display ruby; then
      update-alternatives --set ruby /usr/bin/ruby1.9*
      update-alternatives --set gem /usr/bin/gem1.9*
    fi
} >/dev/null

install_chef_server >/dev/null
install_chef_client >/dev/null
install_berkshelf >/dev/null
clone_chef_repo >/dev/null
wait

berkshelf_run >/dev/null
knife_run >/dev/null
###chef_client_run >/dev/null
wait

echo "Done"

echo 'cd /opt/substructure'
echo './subs zone ${ZONE_FILE} chef sync -b'
echo 'chef-client'
echo './subs zone ${ZONE_FILE} cobbler init -b'
